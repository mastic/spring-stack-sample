package com.example;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Table("T_ORDER_ITEM")
public class OrderItem {

  @Id
  private Long id;
  
  @NotNull
  @Column("AMOUNT")
  @JsonProperty
  private Integer amount;

  @NotNull
  @Column("ORDER_ID")
  @JsonProperty
  private Long orderId;

  @NotNull
  @Column("PRODUCT_ID")
  @JsonProperty
  private Long productId;

  public Long getId() {
    return id;
  }

  
  public Integer getAmount() { return amount; }

  public Long getOrderId() { return orderId; }

  public Long getProductId() { return productId; }

  public void setId(Long id) {
    this.id = id;
  }

  
  public void setAmount(Integer amount) { this.amount = amount; }

  public void setOrderId(Long orderId) { this.orderId = orderId; }

  public void setProductId(Long productId) { this.productId = productId; }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OrderItem orderItem = (OrderItem) o;
    return Objects.equals(id, orderItem.id)
        && Objects.equals(amount, orderItem.amount)
        && Objects.equals(orderId, orderItem.orderId)
        && Objects.equals(productId, orderItem.productId);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("OrderItem{");
    sb.append("id=").append(id);
    sb.append(", amount=").append(amount);
    sb.append(", orderId=").append(orderId);
    sb.append(", productId=").append(productId);
    sb.append('}');
    return sb.toString();
  }

}
