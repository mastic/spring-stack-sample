package com.example;

import static com.example.util.FluxUtils.wrapNotFoundIfEmpty;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/orders")
public class OrderController {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final OrderRepository repository;
  private final CustomerRepository customerRepository;
  private final OrderItemRepository itemsRepository;

  public OrderController(OrderRepository repository, CustomerRepository customerRepository, OrderItemRepository itemsRepository) {
    this.repository = repository;
    this.customerRepository = customerRepository;
    this.itemsRepository = itemsRepository;
  }

  @GetMapping()
  public Flux<Order> listAll() {
    logger.debug("listAll()");
    return repository.findAll();
  }

  @GetMapping("/{id}")
  public Mono<Order> getOne(@PathVariable Long id) {
    logger.debug("getOne({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id);
  }

  @DeleteMapping("/{id}")
  public Mono<Order> delete(@PathVariable Long id) {
    logger.debug("delete({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id)
        .flatMap(order -> repository.delete(order).thenReturn(order)), "order", id);
  }

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<Order> create(@RequestBody @Valid Order order) {
    logger.debug("create({})", order);
    return repository.save(order);
  }

  @PutMapping("/{id}")
  public Mono<Order> update(@PathVariable Long id, @RequestBody @Valid Order entity) {
    logger.debug("create({} : {})", id, entity);
    entity.setId(id);
    return wrapNotFoundIfEmpty(repository.save(entity), "order", id);
  }
  
  @GetMapping("/{id}/customer")
  public Mono<Customer> getCustomer(@PathVariable Long id) {
    logger.debug("getCustomer({})", id);
    return wrapNotFoundIfEmpty(repository.findById(id), "order", id)
        .flatMap(order -> {
          Long customerId = order.getCustomerId();
          return wrapNotFoundIfEmpty(customerRepository.findById(customerId), "customer", customerId);
        });
  }
  
  
  @GetMapping("/{id}/items")
  public Flux<OrderItem> listAllItems(@PathVariable Long id) {
    logger.debug("listAllItems({})", id);
    return wrapNotFoundIfEmpty(getOne(id), "order", id)
        .flatMapMany(order -> itemsRepository.findByOrderId(order.getId()));
  }

  @GetMapping("/{id}/items/{itemsId}")
  public Mono<OrderItem> getOneItems(@PathVariable Long id, @PathVariable Long itemsId) {
    logger.debug("getOneItems({}, {})", id, itemsId);
    return wrapNotFoundIfEmpty(getOne(id), "order", id)
        .flatMap(order -> wrapNotFoundIfEmpty(itemsRepository.findById(itemsId), "items", itemsId));
  }

  @DeleteMapping("/{id}/items/{itemsId}")
  public Mono<OrderItem> deleteItems(@PathVariable Long id, @PathVariable Long itemsId) {
    logger.debug("deleteItems({}, {})", id, itemsId);
    return wrapNotFoundIfEmpty(getOne(id), "order", id)
        .flatMap(order -> wrapNotFoundIfEmpty(itemsRepository.findById(itemsId), "items", itemsId))
        .flatMap(items -> itemsRepository.delete(items).thenReturn(items));
  }

  @PostMapping("/{id}/items")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<OrderItem> createItems(@PathVariable Long id, @RequestBody @Valid OrderItem items) {
    logger.debug("createItems({}, {})", id, items);
    return wrapNotFoundIfEmpty(getOne(id), "order", id)
        .flatMap(order -> {
          items.setOrderId(order.getId());
          return itemsRepository.save(items);
        });
  }

  @PutMapping("/{id}/items/{itemsId}")
  public Mono<OrderItem> updateItems(@PathVariable Long id, @PathVariable Long itemsId, @RequestBody @Valid OrderItem items) {
    logger.debug("updateItems({}, {}, {})", id, itemsId, items);
    return wrapNotFoundIfEmpty(getOne(id), "order", id)
        .flatMap(order -> {
          items.setId(itemsId);
          return wrapNotFoundIfEmpty(itemsRepository.save(items), "items", id);
        });
  }
  
}
