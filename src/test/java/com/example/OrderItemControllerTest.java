package com.example;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class OrderItemControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  OrderItemRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    OrderItem entity = new OrderItem();
    entity.setAmount(17983);
    entity.setOrderId(data.getOrder().getId());
    entity.setProductId(data.getProduct().getId());

    OrderItem created = rest.post()
        .uri("/order-items")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(17983, created.getAmount());
    assertEquals(data.getOrder().getId(), created.getOrderId());
    assertEquals(data.getProduct().getId(), created.getProductId());

    OrderItem single = rest.get()
        .uri("/order-items/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<OrderItem> list = rest.get()
        .uri("/order-items")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<OrderItem>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    OrderItem entity = new OrderItem();
    entity.setAmount(17983);
    entity.setOrderId(data.getOrder().getId());
    entity.setProductId(data.getProduct().getId());

    OrderItem created = rest.post()
        .uri("/order-items")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(17983, created.getAmount());
    assertEquals(data.getOrder().getId(), created.getOrderId());
    assertEquals(data.getProduct().getId(), created.getProductId());

    entity.setAmount(18025);
    entity.setOrderId(data.getOrder().getId());
    entity.setProductId(data.getProduct().getId());

    OrderItem updated = rest.put()
        .uri("/order-items/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(18025, updated.getAmount());
    assertEquals(data.getOrder().getId(), updated.getOrderId());
    assertEquals(data.getProduct().getId(), updated.getProductId());

    OrderItem single = rest.get()
        .uri("/order-items/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    OrderItem entity = new OrderItem();
    entity.setAmount(17983);
    entity.setOrderId(data.getOrder().getId());
    entity.setProductId(data.getProduct().getId());

    OrderItem created = rest.post()
        .uri("/order-items")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(17983, created.getAmount());
    assertEquals(data.getOrder().getId(), created.getOrderId());
    assertEquals(data.getProduct().getId(), created.getProductId());

    OrderItem deleted = rest.delete()
        .uri("/order-items/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(OrderItem.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/order-items/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/order-items/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/order-items/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  @Test
  public void getOrder() {
    OrderItem entity = new OrderItem();
    entity.setAmount(17983);
    entity.setOrderId(data.getOrder().getId());
    entity.setProductId(data.getProduct().getId());

    OrderItem orderItem = Objects.requireNonNull(repository.save(entity).block());

    Order result = rest.get()
        .uri("/order-items/{id}/order", orderItem.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidOrder() {
    rest.get()
        .uri("/order-items/{id}/order", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingOrder() {
    rest.get()
        .uri("/order-items/{id}/order", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  @Test
  public void getProduct() {
    OrderItem entity = new OrderItem();
    entity.setAmount(17983);
    entity.setOrderId(data.getOrder().getId());
    entity.setProductId(data.getProduct().getId());

    OrderItem orderItem = Objects.requireNonNull(repository.save(entity).block());

    Product result = rest.get()
        .uri("/order-items/{id}/product", orderItem.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Product.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(result);
    assertEquals(result.getId(), result.getId());
  }

  @Test
  public void getInvalidProduct() {
    rest.get()
        .uri("/order-items/{id}/product", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingProduct() {
    rest.get()
        .uri("/order-items/{id}/product", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
}
