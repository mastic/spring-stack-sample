package com.example;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@ControllerTest
public class CustomerControllerTest {

  @Autowired
  ApplicationContext context;

  @Autowired
  TestData data;

  @Autowired
  CustomerRepository repository;

  WebTestClient rest;

  @BeforeEach
  public void setUp() {
    rest = WebTestClient.bindToApplicationContext(context)
        .configureClient()
        .responseTimeout(Duration.ofHours(1L))
        .build();
  }

  @Test
  public void createGetAndList() {

    Customer entity = new Customer();
    entity.setName("Malayan Tapir");

    Customer created = rest.post()
        .uri("/customers")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());

    Customer single = rest.get()
        .uri("/customers/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<Customer> list = rest.get()
        .uri("/customers")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<Customer>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGet() {

    Customer entity = new Customer();
    entity.setName("Malayan Tapir");

    Customer created = rest.post()
        .uri("/customers")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());

    entity.setName("Malayan Tapir Updated");

    Customer updated = rest.put()
        .uri("/customers/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals("Malayan Tapir Updated", updated.getName());

    Customer single = rest.get()
        .uri("/customers/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGet() {

    Customer entity = new Customer();
    entity.setName("Malayan Tapir");

    Customer created = rest.post()
        .uri("/customers")
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(entity)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals("Malayan Tapir", created.getName());

    Customer deleted = rest.delete()
        .uri("/customers/{id}", created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Customer.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/customers/{id}", created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalid() {
    rest.get()
        .uri("/customers/{id}", "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissing() {
    rest.get()
        .uri("/customers/{id}", -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
  
  @Test
  public void createGetAndListOrders() {
    Order orders = new Order();
    orders.setCustomerId(data.getCustomer().getId());
    orders.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    orders.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    orders.setNotes("Malayan Tapir");

    Order created = rest.post()
        .uri("/customers/{customerId}/orders", data.getCustomer().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(orders)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);

    Order single = rest.get()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());

    List<Order> list = rest.get()
        .uri("/customers/{customerId}/orders", data.getCustomer().getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(new ParameterizedTypeReference<List<Order>>() {
        })
        .returnResult()
        .getResponseBody();

    assertNotNull(list);
    assertThat(list, not(empty()));
    assertThat(created, is(in(list)));
  }

  @Test
  public void createUpdateAndGetOrders() {
    Order orders = new Order();
    orders.setCustomerId(data.getCustomer().getId());
    orders.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    orders.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    orders.setNotes("Malayan Tapir");

    Order created = rest.post()
        .uri("/customers/{customerId}/orders", data.getCustomer().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(orders)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getCustomer().getId(), created.getCustomerId());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getOrderDate());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getDeliveryDate());
    assertEquals("Malayan Tapir", created.getNotes());

    orders.setOrderDate(Instant.parse("2020-02-02T10:00:00.00Z"));
    orders.setDeliveryDate(Instant.parse("2020-02-02T10:00:00.00Z"));
    orders.setNotes("Malayan Tapir Updated");

    Order updated = rest.put()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(orders)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(updated);
    assertEquals(data.getCustomer().getId(), updated.getCustomerId());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getOrderDate());
    assertEquals(Instant.parse("2020-02-02T10:00:00.00Z"), updated.getDeliveryDate());
    assertEquals("Malayan Tapir Updated", updated.getNotes());

    Order single = rest.get()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(single);
    assertEquals(created.getId(), single.getId());
  }

  @Test
  public void createDeleteAndGetOrders() {
    Order orders = new Order();
    orders.setCustomerId(data.getCustomer().getId());
    orders.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    orders.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
    orders.setNotes("Malayan Tapir");

    Order created = rest.post()
        .uri("/customers/{customerId}/orders", data.getCustomer().getId())
        .accept(MediaType.APPLICATION_JSON)
        .bodyValue(orders)
        .exchange()
        .expectStatus()
        .isCreated()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(created);
    assertEquals(data.getCustomer().getId(), created.getCustomerId());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getOrderDate());
    assertEquals(Instant.parse("2020-02-01T10:00:00.00Z"), created.getDeliveryDate());
    assertEquals("Malayan Tapir", created.getNotes());

    Order deleted = rest.delete()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), created.getId())
        .exchange()
        .expectStatus()
        .isOk()
        .expectBody(Order.class)
        .returnResult()
        .getResponseBody();

    assertNotNull(deleted);
    assertEquals(created.getId(), deleted.getId());

    rest.get()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), created.getId())
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }

  @Test
  public void getInvalidOrders() {
    Customer entity = new Customer();
    entity.setName("Malayan Tapir");
    Customer customer = Objects.requireNonNull(repository.save(entity).block());

    rest.get()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), "invalid")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isBadRequest();
  }

  @Test
  public void getMissingOrders() {
    Customer entity = new Customer();
    entity.setName("Malayan Tapir");
    Customer customer = Objects.requireNonNull(repository.save(entity).block());

    rest.get()
        .uri("/customers/{customerId}/orders/{ordersId}", data.getCustomer().getId(), -666L)
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus()
        .isNotFound();
  }
  
}
