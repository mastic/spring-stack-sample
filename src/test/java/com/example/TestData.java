package com.example;

import org.springframework.stereotype.Component;

@Component
public class TestData {

  private Customer customer;
  private Order order;
  private Product product;
  private OrderItem orderItem;

  public Customer getCustomer() {
    return customer;
  }

  public Order getOrder() {
    return order;
  }

  public Product getProduct() {
    return product;
  }

  public OrderItem getOrderItem() {
    return orderItem;
  }


  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public void setOrderItem(OrderItem orderItem) {
    this.orderItem = orderItem;
  }


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("TestData {\n");
    sb.append(" customer=").append(customer).append("\n");
    sb.append(" order=").append(order).append("\n");
    sb.append(" product=").append(product).append("\n");
    sb.append(" orderItem=").append(orderItem).append("\n");
    sb.append('}');
    return sb.toString();
  }

}
