package com.example;

import java.time.Instant;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public class TestDataSetup implements BeforeAllCallback {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final Object lock = new Object();
  private static boolean initialized = false;

  @Override
  public void beforeAll(ExtensionContext context) {
    synchronized (lock) {
      if (!initialized) {
        initialized = true;
        logger.info("initializing test data...");

        final ApplicationContext ctx = SpringExtension.getApplicationContext(context);
        final TestData data = ctx.getBean(TestData.class);
        logger.debug("initial test data: {}", data);

        final CustomerRepository customerRepository = ctx.getBean(CustomerRepository.class);
        logger.debug("creating Customer...");
        final Customer customer = new Customer();
        customer.setName("Malayan Tapir");
        data.setCustomer(customerRepository.deleteAll()
            .then(customerRepository.save(customer))
            .block());
        logger.info("test Customer: {}", data.getCustomer());

        final OrderRepository orderRepository = ctx.getBean(OrderRepository.class);
        logger.debug("creating Order...");
        final Order order = new Order();
        order.setOrderDate(Instant.parse("2020-02-01T10:00:00.00Z"));
        order.setDeliveryDate(Instant.parse("2020-02-01T10:00:00.00Z"));
        order.setNotes("Malayan Tapir");
        order.setCustomerId(data.getCustomer().getId());
        data.setOrder(orderRepository.deleteAll()
            .then(orderRepository.save(order))
            .block());
        logger.info("test Order: {}", data.getOrder());

        final ProductRepository productRepository = ctx.getBean(ProductRepository.class);
        logger.debug("creating Product...");
        final Product product = new Product();
        product.setName("Malayan Tapir");
        product.setCode("Malayan Tapir");
        product.setType(ProductType.values()[0]);
        data.setProduct(productRepository.deleteAll()
            .then(productRepository.save(product))
            .block());
        logger.info("test Product: {}", data.getProduct());

        final OrderItemRepository orderItemRepository = ctx.getBean(OrderItemRepository.class);
        logger.debug("creating OrderItem...");
        final OrderItem orderItem = new OrderItem();
        orderItem.setAmount(17983);
        orderItem.setOrderId(data.getOrder().getId());
        orderItem.setProductId(data.getProduct().getId());
        data.setOrderItem(orderItemRepository.deleteAll()
            .then(orderItemRepository.save(orderItem))
            .block());
        logger.info("test OrderItem: {}", data.getOrderItem());

        logger.info("test data initialized: {}", data);
      }
    }
  }

}
